=== WPGancio ===
Contributors: lesion
Donate link: https://gancio.org
Tags: events, gancio, fediverse, AP, activity pub
Requires at least: 4.7
Tested up to: 5.7.2
Stable tag: 1.0
Requires PHP: 7.0
License: AGPLv3 or later
License URI: https://www.gnu.org/licenses/agpl-3.0.html

Connect a gancio instance to a Wordpress user so that published events are automatically pushed.

== Description ==

This plugin connects a [Gancio](https://gancio.org) instance to a Wordpress website to automatically push events published on Wordpress.

It requires an event manager plugin, only [Event Organiser[(https://wp-event-organiser.com/) is supported but adding another plugin it's an easy task.
 

== Changelog ==

= 1.0 =
* First release
